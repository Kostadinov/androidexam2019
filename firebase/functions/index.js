const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();
const dbRoot = admin.database().ref();


// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//

exports.calculate = functions.https.onRequest((request, response) => {
	// Check for POST request
	if(request.method !== "POST"){
	 response.status(400).send('Please send a POST request');
	 return;
	}
	
	let data = request.body;
	let filename = data.filename;
	let size = data.filesize;
	let order = createOrder(filename, size, new Date().getTime());
	response.status(200).send(order);
});

exports.makeOrder = functions.https.onRequest((request, response) => {
	// Check for POST request
	if(request.method !== "POST"){
	 response.status(400).send('Please send a POST request');
	 return;
	}
	
	let data = request.body;
	let userId = data.userId
	let filename = data.filename;
	let size = data.filesize;
	
	admin.auth().getUser(userId).then((userRecord) => {
			// See the UserRecord reference doc for the contents of userRecord.
			console.log('Successfully fetched user data:', userRecord.toJSON());
			
			let order = createOrder(filename, size, new Date().getTime());
			var newOrder = dbRoot.child('users/' + userId + '/orders').push();
			newOrder.set(order);
			response.status(200).send(order);
			
			return;
	})
	.catch((error) => {
			console.log('Error fetching user data:', error);
			response.status(404).send('No user with this id exists');
			return;
	});	
});

exports.getAllOrders = functions.https.onRequest((request, response) => {
	// Check for POST request
	if(request.method !== "POST"){
	 response.status(400).send('Please send a POST request');
	 return;
	}
	
	let data = request.body;
	let userId = data.userId
	

	dbRoot.child('users/' + userId + '/orders').once('value').then((snapshot) => {
		var list = new Array();
		snapshot.forEach(function(childSnapshot) {
			var childData = childSnapshot.val();
			list.push(childData);
		});
		response.status(200).send(list);
		return;
	})
	.catch((error) => {
		console.log('Error fetching user data:', error);
		response.status(404).send('No user with this id exists');
	});
				
});


function createOrder(filename, size, createdOn) {
	let prizeCalc = filename.length * 0.03 + (size / 100);
	let completionTimeMillis = Math.round((prizeCalc / 0.5) * 60 * 60 * 1000);
	return {
		filename: filename,
		filesize: size,
		prize: prizeCalc,
		createdOn: createdOn,
		completionTime: completionTimeMillis
	};
}