package com.vsc.softuniexam2019.data.remote.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vsc.softuniexam2019.FileUtils;
import com.vsc.softuniexam2019.data.local.Order;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiWrapper {

    private static ApiWrapper instance;
    private final OrdersService service;


    public static ApiWrapper getInstance() {
        if(instance == null) instance = new ApiWrapper();
        return instance;
    }

    private ApiWrapper() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(OrdersService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();

        service = retrofit.create(OrdersService.class);
    }

    public void calculateOrder(String fileName, long fileSizeInBytes, DataListener<Order> listener) {
        service.calculateOrder(new OrderRequest(fileName, FileUtils.bytesToKiloBytes(fileSizeInBytes))).enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                if(response.isSuccessful()) {
                    listener.onDataReceived(Order.fromOrderResponse(response.body()));
                } else {
                    listener.onDataReceived(null);
                }
            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                listener.onDataReceived(null);
            }
        });
    }

    public void makeOrder(String userId, String fileName, long fileSizeInBytes, DataListener<Order> listener) {
        service.makeOrder(new OrderRequest(userId, fileName, FileUtils.bytesToKiloBytes(fileSizeInBytes))).enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                if(response.isSuccessful()) {
                    listener.onDataReceived(Order.fromOrderResponse(response.body()));
                } else {
                    listener.onDataReceived(null);
                }
            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                listener.onDataReceived(null);
            }
        });
    }

    public void getAllOrders(String userId, DataListener<List<Order>> listener) {
        service.getAllOrders(new OrderRequest(userId)).enqueue(new Callback<List<OrderResponse>>() {
            @Override
            public void onResponse(Call<List<OrderResponse>> call, Response<List<OrderResponse>> response) {
                if(response.isSuccessful()) {
                    listener.onDataReceived(Order.listFromOrderResponses(response.body()));
                } else {
                    listener.onDataReceived(null);
                }
            }

            @Override
            public void onFailure(Call<List<OrderResponse>> call, Throwable t) {
                listener.onDataReceived(null);
            }
        });
    }

    public interface DataListener<T> {
        void onDataReceived(T data);
    }
}
