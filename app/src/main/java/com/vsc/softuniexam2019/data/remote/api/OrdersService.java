package com.vsc.softuniexam2019.data.remote.api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface OrdersService {

    public static final String BASE_URL = "https://us-central1-soft-uni-demo.cloudfunctions.net/";

    @POST("calculate")
    public Call<OrderResponse> calculateOrder(@Body OrderRequest request);

    @POST("makeOrder")
    public Call<OrderResponse> makeOrder(@Body OrderRequest request);

    @POST("getAllOrders")
    public Call<List<OrderResponse>> getAllOrders(@Body OrderRequest request);
}
