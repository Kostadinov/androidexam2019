package com.vsc.softuniexam2019.ui.fragments;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;
import com.vsc.softuniexam2019.data.remote.AuthService;
import com.vsc.softuniexam2019.FileUtils;
import com.vsc.softuniexam2019.R;
import com.vsc.softuniexam2019.data.local.Order;
import com.vsc.softuniexam2019.data.remote.api.ApiWrapper;
import com.vsc.softuniexam2019.databinding.FragmentNewOrderBinding;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.app.Activity.RESULT_OK;

public class NewOrderFragment extends Fragment {

    private static final int RQ_GALLERY = 44;
    private static NewOrderFragment instance;

    private FragmentNewOrderBinding binding;
    private String imageName;
    private long imageSizeInBytes;

    public static NewOrderFragment newInstance() {
        if(instance == null) instance = new NewOrderFragment();
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_new_order, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.btnGetPhoto.setOnClickListener(v -> onNewPhotoClicked());
        binding.btnCalc.setOnClickListener(v -> onCalculateOrderClicked());
        binding.btnOrder.setOnClickListener(v -> onOrderClicked());
    }

    private void onOrderClicked() {
        binding.progressOrder.setVisibility(View.VISIBLE);
        binding.btnOrder.setEnabled(false);
        ApiWrapper.getInstance().makeOrder(AuthService.getInstance().getLoggedUser().getId(), imageName, imageSizeInBytes, data -> onOrderAccpeted(data));
    }

    private void onOrderAccpeted(Order data) {
        binding.progressOrder.setVisibility(View.GONE);
        binding.btnOrder.setEnabled(true);
        if(data != null) {
            resetViews();
            showMessage("Order accepted!");
        } else {
            showMessage("Ordering failed!");
        }
    }

    private void onNewPhotoClicked() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, RQ_GALLERY);
        resetViews();
    }

    private void onCalculateOrderClicked() {
        binding.progressBar.setVisibility(View.VISIBLE);
        binding.btnCalc.setEnabled(false);
        ApiWrapper.getInstance().calculateOrder(imageName, imageSizeInBytes, data -> showEstimatedOrder(data));
    }

    private void showEstimatedOrder(Order order) {
        binding.progressBar.setVisibility(View.GONE);
        binding.btnCalc.setEnabled(true);
        if(order != null) {
            binding.txtPrice.setText("$" + order.getPrice());
            binding.txtDeliveryDate.setText(calculateDeliveryDate(order.getCompletionTime()));
            binding.grpPart3.setVisibility(View.VISIBLE);
        } else {
            showMessage("Calculation failed!");
        }
    }

    private void resetViews() {
        imageName = "";
        imageSizeInBytes = 0;
        binding.progressBar.setVisibility(View.GONE);
        binding.progressOrder.setVisibility(View.GONE);
        binding.btnCalc.setEnabled(true);
        binding.grpPart2.setVisibility(View.GONE);
        binding.grpPart3.setVisibility(View.GONE);
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);


        if (reqCode == RQ_GALLERY && resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getActivity().getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                onImageSelected(imageUri, selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
            }
        }else {
            Toast.makeText(getActivity(), "You haven't picked Image",Toast.LENGTH_LONG).show();
        }
    }

    private void onImageSelected(Uri imageUri, Bitmap image) {
        this.imageName = FileUtils.getFileName(imageUri, getActivity());
        this.imageSizeInBytes = Long.parseLong(FileUtils.getFileSize(imageUri, getActivity()));

        binding.imageView.setImageBitmap(image);
        binding.txtFileName.setText(imageName);
        binding.txtFileSize.setText(FileUtils.bytesToKiloBytes(imageSizeInBytes) + " KB");

        binding.grpPart2.setVisibility(View.VISIBLE);
    }

    private String calculateDeliveryDate(long completionTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
        Date date = new Date(System.currentTimeMillis() + completionTime);
        return sdf.format(date);
    }


    private void showMessage(String text) {
        Snackbar.make(binding.getRoot(), text, Snackbar.LENGTH_LONG).show();
    }
}
