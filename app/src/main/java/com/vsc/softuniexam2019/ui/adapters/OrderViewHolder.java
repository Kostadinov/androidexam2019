package com.vsc.softuniexam2019.ui.adapters;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vsc.softuniexam2019.R;
import com.vsc.softuniexam2019.data.local.Order;
import com.vsc.softuniexam2019.databinding.ListItemOrderBinding;

import java.text.SimpleDateFormat;
import java.util.Date;

class OrderViewHolder extends RecyclerView.ViewHolder {

    private ListItemOrderBinding binding;
    private static SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");

    OrderViewHolder(ListItemOrderBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    void setData(Order order) {
        binding.txtFileName.setText(order.getFileName());
        binding.txtFileSize.setText(order.getFileSize() + " KB");
        binding.txtPrice.setText(String.format("$%.2f", Double.parseDouble(order.getPrice())));

        long deliveryTimestamp = order.getCreatedOn() + order.getCompletionTime();
        Date deliveryDate = new Date(deliveryTimestamp);
        if(System.currentTimeMillis() < deliveryTimestamp) {
            binding.txtDelivery.setText("Delivery by " + sdf.format(deliveryDate));
            binding.txtDelivery.setBackgroundColor(binding.getRoot().getContext().getResources().getColor(R.color.colorAccent));
        } else {
            binding.txtDelivery.setText("Delivered on " + sdf.format(deliveryDate));
            binding.txtDelivery.setBackgroundColor(binding.getRoot().getContext().getResources().getColor(R.color.grey));
        }
    }
}