package com.vsc.softuniexam2019.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.vsc.softuniexam2019.R;
import com.vsc.softuniexam2019.data.local.Order;
import com.vsc.softuniexam2019.databinding.ListItemOrderBinding;

import java.util.List;

public class OrdersAdapter extends RecyclerView.Adapter<OrderViewHolder>  {

    private List<Order> data;

    public OrdersAdapter(List<Order> data){
        this.data = data;
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ListItemOrderBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.list_item_order, parent, false);
        return new OrderViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, int position) {
        holder.setData(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


}
